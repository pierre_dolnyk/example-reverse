% rev(L, R) = R is reversed L

% rev(List, ReversedAccumulator, Accumulator, Reversed) = reversed List concatenated with Accumulator is Reversed
% we use accumulator to get O(N) complexity instead of O(N^2) 
% ReversedAccumulator is a safe guard that will allow us to write rev predicate with the first argument being an 
% variable, for example rev(X, [1,2,3]) will give exactly one solution: X = [3,2,1]
rev(List, Reversed) :-
    rev(List, Reversed, [], Reversed).
rev([], [], Reversed, Reversed).
rev([Head | Tail], [_ | ReversedTail], Accumulator, Reversed) :-
    rev(Tail, ReversedTail, [Head | Accumulator], Reversed).
